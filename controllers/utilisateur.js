const { ObjectID } = require("bson");
const client = require("../db/connect");
const { Utilisateur } = require("../models/utilisateur");
const jwt = require('jsonwebtoken');

const generateToken = (result, isPresentIndex) => {
  return jwt.sign(
    result[isPresentIndex],
    process.env.JWT_SECRET_KEY
  );
}

const verifyToken = (req) => {
  // Get token value to the json body
  const token = req.body.token;
 
  // If the token is present
  if(token){
      // Verify the token using jwt.verify method
      const decode = jwt.verify(token, process.env.JWT_SECRET_KEY);

      return decode;
  }else{
    return false;
  }

}

const ajouterUtilisateur = async (req, res) => {
  try {
    let utilisateur = new Utilisateur(
      req.body.email,
      req.body.password
    );
    let result = await client
      .db()
      .collection("utilisateurs")
      .insertOne(utilisateur);

    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(501).json(error);
  }
};

const login = async (req, res) => {
  // Get the name to the json body data
  const email = req.body.email;
 
  // Get the password to the json body data
  const password = req.body.password;

  // Make two variable for further use
  let isPresent = false;
  let isPresentIndex = null;
  let isPresentId = null;

  let cursor = client
      .db()
      .collection("utilisateurs")
      .find()
      .sort({ email: 1 });
    let result = await cursor.toArray();

  // Iterate a loop to the data items and
  // check what data are method
  for(let i=0; i<result.length; i++){

      // If data name are matched so check
      // the password are correct or not
      if(result[i].email === email &&
        result[i].password === password){

          isPresentId = result[i]._id;

          // If both are correct so make
          // isPresent variable true
          isPresent = true;

          // And store the data index
          isPresentIndex = i;

          // Break the loop after matching
          // successfully
          break;
      }
  }

  // If isPresent is true, then create a
  // token and pass to the response
  if(isPresent){

      // The jwt.sign method are used
      // to create token
      const token = generateToken(result, isPresentIndex);
       
      // Pass the data or token in response
      res.json({
          login: true,
          id:isPresentId,
          token: token
      });
  }else{
      // If isPresent is false return the error
      res.json({
          login: false,
          error: 'please check name and password.'
      });
  }
}

const getUtilisateurs = async (req, res) => {
  try {
    let cursor = client
      .db()
      .collection("utilisateurs")
      .find()
      .sort({ email: 1 });
    let result = await cursor.toArray();
    if (result.length > 0) {
      res.status(200).json(result);
    } else {
      res.status(204).json({ msg: "Aucun utilisateur trouvé" });
    }
  } catch (error) {
    console.log(error);
    res.status(501).json(error);
  }
};

const getUtilisateur = async (req, res) => {
  try {
    let id = new ObjectID(req.params.id);
    let cursor = client.db().collection("utilisateurs").find({ _id: id });
    let result = await cursor.toArray();
    if (result.length > 0) {
      res.status(200).json(result[0]);
    } else {
      res.status(204).json({ msg: "Cet utilisateur n'existe pas" });
    }
  } catch (error) {
    console.log(error);
    res.status(501).json(error);
  }
};

const updateUtilisateur = async (req, res) => {
  let verif = verifyToken(req);

  console.log(verif.header);

  if(verif !== false){
    try {
      let id = new ObjectID(req.params.id);
      let email = req.body.email;
      let password = req.body.password;
      let result = await client
        .db()
        .collection("utilisateurs")
        .updateOne({ _id: id }, { $set: { email, password} });
  
      if (result.modifiedCount === 1) {
        res.status(200).json({ msg: "Modification réussie" });
      } else {
        res.status(404).json({ msg: "Cet utilisateur n'existe pas ou aucune modification n'a été nécessaire" });
      }
    } catch (error) {
      console.log(error);
      res.status(501).json(error);
    }

    //  Return response with decode data
    /*
    res.json({
      login: true,
      data: decode
    });
    */
   return;
  }

  // Else : Return response with error
  res.json({
    login: false,
    data: 'error'
  });
};

const deleteUtilisateur = async (req, res) => {
  try {
    let id = new ObjectID(req.params.id);
    let result = await client
      .db()
      .collection("utilisateurs")
      .deleteOne({ _id: id });
    if (result.deletedCount === 1) {
      res.status(200).json({ msg: "Suppression réussie" });
    } else {
      res.status(404).json({ msg: "Cet utilisateur n'existe pas" });
    }
  } catch (error) {
    console.log(error);

    res.status(501).json(error);
  }
};

module.exports = {
  ajouterUtilisateur,
  login,
  getUtilisateurs,
  getUtilisateur,
  updateUtilisateur,
  deleteUtilisateur,
};
