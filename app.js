const express = require("express");
const { connect } = require("./db/connect");
const routerUtilisateurs = require("./routers/utilisateur");
const app = express();
const dotenv = require('dotenv');
let cors = require('cors'); // Utilisé pour pouvoir faire des appels depuis un autre host.

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use("/api/v1", routerUtilisateurs);

// Set up Global configuration access
dotenv.config();

let PORT = process.env.PORT || 3001;

connect("mongodb://mongo:27017/", (err) => {
  if (err) {
    console.log("Erreur lors de la connexion à la base de données");
    console.log(err);
    process.exit(-1);
  } else {
    console.log("Connexion avec la base de données établie");
    app.listen(PORT);
    console.log("Attente des requêtes au port 3OO1");
  }
});
